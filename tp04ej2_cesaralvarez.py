def MENU():
    print("      MENU          ")
    print("a:Cargar productos")
    print("b:Mostrar el listado de productos")
    print("cMostrar los productos cuyo stock se encuentre en el intervalo:")
    print("d:agrgar stock")
    print("e:Eliminar todos los productos cuyo stock sea igual a cero")
    print("f:Salir")
    respuesta=input("iNGRESE SU OPCION")
    return respuesta

def CARGARPRODUCTOS(productos):
    codigo=-1
    while(codigo!=0):
     codigo=int(input("ingrese codigo del producto/ ingrese 0 para no ingresar mas"))
     if (codigo not in productos and codigo!=0):
            descripcion=input("ingrese la descripcion del producto")
            if (descripcion!=""):
             precio=int(input("ingrese precio del producto"))
             if (precio>0):
                stock=int(input("ingrese el stock del producto"))
                if (stock>=0):
                    productos[codigo]=[descripcion, precio, stock]
                else:
                    print("stock invalido")    
             else:
                  print("descripcion invalida")
            else:
                print("descripcion invalida")
     elif codigo==0:
            print("hasta luego")
     else:
          print("codigo repetido")
     
    #codigo=input("ingrese codigo del producto/ ingrese 0 para no ingresar mas")  
    return productos 

def MOSTRARLISTA(productos):
    for codigo, valor in productos.items():
        print(codigo,valor)

def MOSTRARINTERVALO(productos):
    stock1=int(input("ingrese el primer valor del intervalo"))
    stock2=int(input("ingrese el segundo valor del intervalo"))
    for codigo, valor in productos.items():
     if (valor[2]>=stock1 and valor[2]<=stock2):
      print(codigo,valor)
def AGREGARSTOCK(productos):   
    agregar=int(input("ingrese stock a agregar a los productos")) 
    stockY=int(input("ingrese stock a modificar"))
    for codigo, valor in productos.items():
     if (valor[2]<=stockY):
         valor[2]=agregar

def buscar(productos):
    cero=0
    for codigo, valor in productos.items():
        if (valor[2]==0):
         cero=codigo  
    return cero 
def ELIMINAR(productos):
        while(buscar(productos)!=0): 
           del productos[buscar(productos)]
           print("Producto eliminado")


#principal

productos={}
opcion=''
while(opcion!='f'):
    opcion=MENU()
    if (opcion=='a'):
       productos=CARGARPRODUCTOS(productos)
    elif(opcion=='b'):
       MOSTRARLISTA(productos)
    elif(opcion=='c'):
       MOSTRARINTERVALO(productos)
    elif(opcion=='d'):
      AGREGARSTOCK(productos)
    elif(opcion=='e'):
        ELIMINAR(productos)
    elif(opcion=='f'):
       print("hasta luego")
    else:
        print("incorrecto")